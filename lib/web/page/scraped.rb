# encoding: UTF-8

require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'pathname'

module WebScraper

  class PageScraped

    include WebScraper    

    def initialize(root, path, local)
      url = File.join(root, path)
      begin
        @data = Nokogiri::HTML(open(url))
        @path = is_dot_dir?(path) ? Pathname.new("/#{WebScraper::HTML_DEFAULT_PAGE}") : path   
        @filename = File.join(local, root.gsub(%r{^.*:\/\/}, ''), "#{@path}#{WebScraper::HTML_DEFAULT_EXT}")
      rescue OpenURI::HTTPError => e
        exception_for(url, e)
      end
    end

    def exception_for(url, exception)
      @exceptions = [] unless @exceptions
      @exceptions << { :url => url, :e => exception }
    end

    def log_exceptions
      @exceptions.each { |e|
        logger.error("EXCEPTIONS FOR URL: #{e[:url]}\n#{e[:e]}")
      }
    end

    def process_image_data_interchange_and_result(element)
      href = element.attribute('data-interchange').value
      r = /\[(.*?)\s?,\s?(\(\w+\))\]\s?/mi
      fragments = href.scan(r)
      images = fragments.map { |frag|
        unless excluded?(frag[0], %r{^.*:\/\/|^#}) 
          path = clean_from_params(relativize(frag[0]))
          nhref = prepend_dots(@path)
          nhref = nhref.empty? ? path.to_s : File.join(nhref, path.to_s)
          href.gsub!(frag[0], nhref)
          rel_path = Pathname.new(nhref.gsub(/^(\.\.\/)+/, ''))
          #[File.extname(nhref), rel_path , href]
          [File.extname(nhref), rel_path, rel_path.to_s]
        end
      }
      element.attribute('data-interchange').value = href
      images
    end
    private :process_image_data_interchange_and_result

    def default_processing(href)
      href = relativize(href)
      href = clean_from_params(href)
      dots = prepend_dots(@path)
      nhref = dots.empty? ? href.to_s : File.join(dots, href.to_s)
      [href, nhref]
    end
    private :default_processing

    def asset_processing(href)
      asset = File.join(WebScraper::ASSETS_PATH, clean_from_params(href, :domain => true))
      dots = prepend_dots(@path)
      nhref = dots.empty? ? asset : File.join(dots, asset.to_s)
      href = Pathname.new(href)
      [href, nhref]
    end
    private :asset_processing

    def google_font_processing(href)
      asset = File.join(WebScraper::ASSETS_PATH, "#{href.to_slug}.css")        
      dots = prepend_dots(@path)
      nhref = dots.empty? ? asset : File.join(dots, asset.to_s)
      href = Pathname.new(href)
      [href, nhref]
    end
    private :google_font_processing

    def process_attribute_and_result(element, href, attr, type_of = :anchor)
      href = "#{WebScraper::HTML_DEFAULT_PAGE}#{WebScraper::HTML_DEFAULT_EXT}" if is_root_dir?(href)
      
      case type_of
        
      when :anchor
        href, nhref = default_processing(href)
        ext = File.extname(href)
        unless ext.empty?
          element.attribute(attr).value = nhref
          [ext, href, element.attribute(attr).value]
        else
          unless is_dot_dir?(href)
            element.attribute(attr).value = "#{nhref}#{WebScraper::HTML_DEFAULT_EXT}"
            [WebScraper::HTML_DEFAULT_EXT, href, element.attribute(attr).value]
          end
        end
        
      when :link
        href, nhref = default_processing(href)
        element.attribute(attr).value = nhref
        extname = href.extname.empty? ? '.css' : href.extname
        [extname, href, nhref]
        
      when :link_google_fonts
        href, nhref = google_font_processing(href)
        element.attribute(attr).value = nhref
        extname = href.extname.empty? ? '.css' : href.extname
        [extname, href, nhref]
        
      when :script_external, :link_external
        href, nhref = asset_processing(href)
        element.attribute(attr).value = nhref
        [href.extname, href, nhref.gsub(%r{^(\.\.\/)*}, '')]
        
      else
        href, nhref = default_processing(href)
        element.attribute(attr).value = nhref
        [href.extname, href, element.attribute(attr).value]
      end    
    end
    private :process_attribute_and_result

    def links    
      (@data.css("link[rel=stylesheet]").map { |a|
         href = a.attribute('href').value
         href = "http:#{href}" if href.match(%r{^\/\/})
         if is_google_font?(href)
           process_attribute_and_result(a, href, 'href', :link_google_fonts)
         else
           process_attribute_and_result(a, href, 'href', has_protocol?(href) ? :link_external : :link) unless is_dot_dir?(href)
         end
        }).compact
    end
    private :links

    def scripts
      (@data.css("script[src]").map { |a|
         href = a.attribute('src').value
         href = "http:#{href}" if href.match(%r{^\/\/})
         process_attribute_and_result(a, href, 'src',has_protocol?(href) ? :script_external : :script) unless is_dot_dir?(href)
        }).compact
    end
    private :scripts

    def images
      
      images_src = @data.css("img[src]").map { |a|
        href = a.attribute('src').value
        process_attribute_and_result(a, href, 'src', :image) unless excluded?(href, PROTOCOL_MATCHER)   
      }
      
      images_data_interchange = @data.css("img[data-interchange]").map { |a|
        process_image_data_interchange_and_result(a)
      }
      (images_data_interchange.flatten(1) + images_src).compact
    end
    private :images

    def hrefs
      refs = (@data.css('a').map { |a|
                href = a.attribute('href')
                if href && !excluded?(href.value, %r{^.*:\/\/|^#|^mailto:})
                  process_attribute_and_result(a, href.value, 'href', :anchor)
                end
        }).compact    
      refs
    end
    private :hrefs

    attr_reader :filename
    attr_reader :data
    attr_reader :exceptions

    def references
      order_and_compact(hrefs + images + links + scripts)
    end
    
  end

end

