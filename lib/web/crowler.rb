# encoding: UTF-8

require 'rubygems'
require 'open-uri'
require 'logger'
require 'net/http'
require 'pathname'
gem 'sass'
require 'sass'

module WebScraper
  
  class Crowler

    include WebScraper

    PROT_REGEX = (%r{^.*:\/\/})

    def initialize(root, local = WebScraper::BASE_DATA_DIR, options = {})
      @local = local
      Dir.mkdir(@local) unless File.exists?(@local)
      @root = root.match(PROT_REGEX) ? root : "http://#{root}"
      @data_dir = File.join(@local, @root.gsub(PROT_REGEX, ''))
      @traversed = []
      @options = options
      @options.reverse_merge!({
        :postprocess_with_scss => false
      })
    end

    def traversed(s)
      h = s.to_s.to_sha1_hash
      @traversed << h unless @traversed.include?(h)
      #@traversed.uniq!
    end
    private :traversed

    def traversed?(s)
      @traversed.include?(s.to_s.to_sha1_hash)
    end
    private :traversed?

    def save_page(page)
      filename = page.filename
      data = page.data
      FileUtils.mkdir_p(File.dirname(filename))
      logger.info("STORE: #{filename}")
      File.open(filename, 'w') {|f|
        data.write_html_to(f, :encoding => 'UTF-8', :indent => 2)
      }
    end
    private :save_page

    def process_css(filename, options = { :related_domain => nil} )
      logger.info("POST PROCESSING CSS: #{filename}")
      f = open(filename, 'r+')
      styles = if @options[:postprocess_with_scss]
                 Sass::Engine.new(f.read, :syntax => :scss, :style => :nested).render
               else
                 f.read
               end
      
      binaries = styles.scan(%r{url\(([^)]+)\)}).map { |i|
        image = i[0].gsub(/['|"]/, '')
        Pathname.new(clean_from_params(image, :protocol => has_protocol?(image))) unless image.match(%r{^data:image})
      }
      binaries.compact!
      binaries.uniq!
      
      binaries.each { |path|
        local_path = unless path.absolute?
                       relativize(Pathname.new(filename.gsub(@data_dir, '')).dirname + path)
                     else
                       relativize(path)
                     end
        if has_protocol?(path)
          np_path = remove_protocol(path)
          styles.gsub!(path.to_s, np_path.to_s)
          download_binary(path, :local_path => File.join(ASSETS_PATH, np_path))
        else
          if options[:related_domain]
            download_binary(local_path.to_s.gsub(ASSETS_PATH, 'http:/'), :local_path => local_path)
          else
            download_binary(local_path)
          end
        end
      }
      f.rewind
      f.write(styles)
      f.close
    end
    private :process_css

    def to_uri(path)
      has_protocol?(path) ? URI(path.to_s) : URI(File.join(@root, path))
    end
    private :to_uri

    def download_binary(path, options = { :local_path => nil, :related_domain => nil} )
      filename = options[:local_path] ? File.join(@data_dir, options[:local_path].to_s) : File.join(@data_dir, path)
      if traversed?(path)
        logger.debug("PREVIOUS PROCESSED: #{path}")
      else
        FileUtils.mkdir_p(File.dirname(filename))
        uri = to_uri(path)
        logger.info("DOWNLOAD: #{uri}")
        Net::HTTP.start(uri.host, uri.port) { |http|
          request = Net::HTTP::Get.new(uri)
          http.request(request) { |response|
            logger.info("STORE: #{filename}")
            open(filename, 'w') { |io|
              response.read_body {  |chunk|
                io.write chunk
              }
            }
          }
        }
        #post processing download
        case File.extname(filename)
        when '.css'
          process_css(filename, :related_domain => options[:related_domain])
        end
      end
      traversed(path)
    end
    private :download_binary

    #TODO: resolve for index.html
    def scrape(path = '')
      url = File.join(@root, path)    
      if traversed?(path)
        logger.debug("PREVIOUS PROCESSED: #{url}")
      else      
        logger.info("PROCESSING: #{url}")
        page = PageScraped.new(@root, path, @local)
        unless page.exceptions
          references = page.references
          save_page(page)
          traversed(path)
          references.each {|ext, href, nhref|
            case ext
            when HTML_DEFAULT_EXT
              scrape(href)
            when CSS_DEFAULT_EXT
              if is_google_font?(href)
                download_binary(href, :local_path => nhref)
              elsif has_protocol?(href)
                download_binary(href, :local_path => nhref, :related_domain => extract_domain(href))
              else
                download_binary(href)
              end
            else
              if has_protocol?(href)
                download_binary(href, :local_path => nhref)#, :related_domain => extract_domain(href))
              else
                download_binary(href)
              end
            end
          }
        else
          traversed(path)
          page.log_exceptions
        end
      end
    end
    private :scrape

    def run!
      logger.info("INIT! SCRAPER #{VERSION} PROCESSING #{@root}...")
      scrape
      logger.info("DONE! PROCESSED #{@traversed.size} URLS\n")
    end
    
  end

end


