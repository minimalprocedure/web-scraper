# encoding: UTF-8

require 'rubygems'

module WebScraper
  class Exec
    require 'optparse'
    
    def initialize(args)
      @options = {:url => nil, :local => './static'}
      set_ops
    end

    def set_ops
      op = OptionParser.new { |opts|
        @opts = opts
        
        opts.banner = %(
        Usage:\tweb-scraper [options]

        Description:
        \tDownload website as best as possible :\).

        Options:
        ).squeeze(' ')

        opts.on("-uURL", "--url=URL", "Website root url") do |u|
          @options[:url] = u
        end

        opts.on("-lLOCAL", "--local=LOCAL", "Local base folder") do |l|
          @options[:local] = l
        end

        opts.on("-h", "--help", "Prints this help") do
          puts opts
          exit
        end
      }
      op.parse!
    end
    
    def run!
      if @options[:url].nil?
         puts @opts
        exit
      end
      scraper = WebScraper::Crowler.new(@options[:url], @options[:local])
      scraper.run!
    end
    
  end 

end
