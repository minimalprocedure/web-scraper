# encoding: UTF-8

require 'rubygems'
require 'pathname'

class String

  require 'digest'

  def to_sha1_hash
    Digest::SHA1.hexdigest(self)
  end

  def to_slug(size = 128, sep = '-')
    chars = %r{[^[A-Za-z0-9]]|\b[aeiou]\b}i
    self.strip.gsub(chars, sep).squeeze(sep).gsub(%r{^-|-$},'').downcase
  end
  
end

class Hash

  def reverse_merge!(other_hash)
    merge!( other_hash ){|key,left,right| left }
  end

end

module WebScraper
  module Helpers

    PROTOCOL_MATCHER = %r{^.*:\/\/|^#}

    def logger
      WebScraper::LOGGER
    end
    private :logger

    def order_and_compact(references)
      refs = (references.sort { |a, b| a[0] <=> b[0] }).uniq { |e| e[1] }
      refs.compact
    end
    private :order_and_compact

    def prepend_dots(path)
      unless is_root_dir?(path.dirname) || is_dot_dir?(path.dirname)
        fragments = path.dirname.to_s.split(File::SEPARATOR).map {|frag| '..' }
        fragments.join(File::SEPARATOR)
      else
        ''
      end
    end
    private :prepend_dots

    def remove_protocol(pname)
      Pathname.new(pname.to_s.gsub(PROTOCOL_MATCHER, ''))
    end
    private :remove_protocol

    def has_protocol?(pname)
      pname.to_s.match(PROTOCOL_MATCHER)
    end
    private :has_protocol?

    def is_root_dir?(pname)
      pname.to_s == '/'
    end
    private :is_root_dir?
    
    def is_sdot_dir?(pname)
      pname.to_s == '.' || pname.to_s.empty?
    end  
    private :is_sdot_dir?

    def is_ddot_dir?(pname)
      pname.to_s == '..' || pname.to_s.empty?
    end  
    private :is_ddot_dir?

    def is_dot_dir?(pname)
      is_sdot_dir?(pname) || is_ddot_dir?(pname)
    end  
    private :is_dot_dir?

    def excluded?(href, regexp)
      href.match(regexp) || is_dot_dir?(href)
    end
    private :excluded?

    def is_google_font?(pname)
      pname.to_s.match('fonts.googleapis.com')
    end
    private :is_google_font?
    
    def relative_fragments(path)
      path = relativize(path)
      path.relative_path_from(path.basename)
    end
    private :relative_fragments

    def relativize(path)
      path = Pathname.new(path) unless path.is_a?(Pathname)
      if path.absolute?
        root = Pathname.new('/')
        path.relative_path_from(root)
      else
        path
      end
    end
    private :relativize

    def clean_from_params(href, options = { :domain => false, :protocol => false })
      uri = URI.parse(href.to_s)
      path = uri.path
      path = File.join(uri.host, uri.path) if options[:domain]
      path = File.join("#{uri.scheme}://#{uri.host}", uri.path) if options[:protocol]
      Pathname.new(path)
    end
    private :clean_from_params

    def extract_domain(href)
      u = URI.parse(href.to_s)
      "#{u.scheme}://#{u.host}"
    end
    
  end
end


