# encoding: UTF-8

dir = File.dirname(__FILE__)
$LOAD_PATH.unshift dir unless $LOAD_PATH.include?(dir)

require 'rubygems'
require 'logger'
require 'pathname'
require 'helpers'

module WebScraper

  VERSION='0.6.0.0'

  LOGGER = Logger.new(STDOUT)
  LOGGER.level = Logger::INFO
  BASE_DATA_DIR='./static'  
  HTML_DEFAULT_EXT = ".html"
  HTML_DEFAULT_PAGE = "index"
  CSS_DEFAULT_EXT = ".css"
  ASSETS_PATH = "assets"
  include WebScraper::Helpers
  
end

require 'page/scraped'
require 'crowler'
require 'exec'
